/**
 * Include your custom JavaScript here.
 *
 * We also offer some hooks so you can plug your own logic. For instance, if you want to be notified when the variant
 * changes on product page, you can attach a listener to the document:
 *
 * document.addEventListener('variant:changed', function(event) {
 *   var variant = event.detail.variant; // Gives you access to the whole variant details
 * });
 *
 * You can also add a listener whenever a product is added to the cart:
 *
 * document.addEventListener('product:added', function(event) {
 *   var variant = event.detail.variant; // Get the variant that was added
 *   var quantity = event.detail.quantity; // Get the quantity that was added
 * });
 *
 * If you are an app developer and requires the theme to re-render the mini-cart, you can trigger your own event. If
 * you are adding a product, you need to trigger the "product:added" event, and make sure that you pass the quantity
 * that was added so the theme can properly update the quantity:
 *
 * document.documentElement.dispatchEvent(new CustomEvent('product:added', {
 *   bubbles: true,
 *   detail: {
 *     quantity: 1
 *   }
 * }));
 *
 * If you just want to force refresh the mini-cart without adding a specific product, you can trigger the event
 * "cart:refresh" in a similar way (in that case, passing the quantity is not necessary):
 *
 * document.documentElement.dispatchEvent(new CustomEvent('cart:refresh', {
 *   bubbles: true
 * }));
 */

const sliderNextArrowButton = document.querySelector('.slider-next-arrow');
if (sliderNextArrowButton) {
  const flkty = Flickity('.slideshow');
  if (flkty) {
    sliderNextArrowButton.onclick = function() {
      flkty.next();
    }
  }
}

// Center hanni logo no matter what.
function centerHanniLogo() {
  const logo = $('.header__logo');

  if (logo && isLaptopAndUp()) {
    logo.css("transition","all 200ms");
    logo.css("position","absolute");
    logo.css("left", Math.max(0, (($(window).width() - logo.outerWidth()) / 2) + $(window).scrollLeft()) + "px");
  } else {
    logo.css("position", "unset");
    logo.css("left", "unset");
  }
}

function positionArrow() {
  const arrow = $('.slider-next-arrow');
  const dots = $('.flickity-page-dots');

  if (arrow.length && dots.length) {
    const widthWindow = $(window).width();
    const { left: dotX } = dots.offset();
    const widthDot = dots.outerWidth();
    const widthPadding = widthWindow * 0.06// 6 vw;
    let value = Math.max(0, (widthWindow - (dotX + widthDot)) - widthPadding - 34);
    if (window.innerWidth > 500) {
      value += 18;
    }
    arrow.css("right", `${value}px`);
  }
}

// Full title br tag correction
function correctBrTag() {
  const pTag = $('.display-br p');
  if (pTag.length && isMobile()) {
    pTag[0].innerHTML = pTag[0].innerHTML.replace('<br> <br>', '<br><br>').replace(/([\w|\s])(<br>)([\w|\s])/g, '$1$3');
  }
}

function isMobile() {
  return window.innerWidth <= 640;
}

function isLaptopAndUp() {
  return window.innerWidth > 1000;
}

$(document).ready(function() {
  positionArrow();
  correctBrTag();
  // Resize event handler.
  $(window).resize(function() {
    centerHanniLogo();
    positionArrow();
    correctBrTag();
  });

  const heightBtn = $(".section-slideshow-products").find('.product-item__action-button').innerHeight();
  const marginBottom = Number($(".section-slideshow-products").css('margin-top')?.replace('px', ''));

  //  Hover Script  [ Product-item.liquid }
  $(".product-item").mouseover(function() {
    if (isLaptopAndUp()) {
      $(this).find("div .imgPpal").hide();
      $(this).find("div .hover-gif").show();
    }

    if($(this).find("button.product-item__action-button.button.button--small.button--primary").length > 0){
      $(this).find("button.product-item__action-button.button.button--small.button--primary").show();
    } else{
      $(this).find("button.button--ternary").show();
      $(this).find(".product-item__action-button").css("display","block");
    }
    $(this).find(".product-item__info").css("margin-top","0");


    $(".section-slideshow-products").css('margin-bottom', marginBottom - heightBtn);

  })
  .mouseout(function() {
    if (isLaptopAndUp()) {
      $(this).find("div .imgPpal").show();
      $(this).find("div .hover-gif").hide();
    }
    $(this).find("button.product-item__action-button.button.button--small.button--primary").hide();
    $(this).find("button.button--ternary").hide();
    $(this).find(".product-item__action-button").css("display","none");
    $(this).find(".product-item__info").css("margin-top","0");

    $(".section-slideshow-products").css('margin-bottom', marginBottom);
  });

});

//  $(window).on("load", function() {
//   var artHeight = $('div#shopify-section-16291428945e610201 .slideshow__slide').outerHeight();
//   $('div#shopify-section-16291428945e610201 .flickity-viewport').css({
//     'height' : artHeight
//   });
// });

$( window ).resize(function() {
//   $( "div#shopify-section-16291428945e610201" ).css("margin-bottom",0);
  $( "div#shopify-section-16291428945e610201   .mobilebtnSlider" ).css("margin-top","15px");
  $( "div#shopify-section-16291428945e610201   .flickity-page-dots" ).css("bottom","80px");
});

   setTimeout(function(){
     $( "div#shopify-section-16291428945e610201 .slideshow__content-wrapper").fadeTo("slow", 1);
     }, 500);

     function addItemToCart(variant_id, qty, removekey) {
      var data = {
        "id": variant_id,
        "quantity": qty
      }
  
      $.ajax({
        type: 'POST',
        url: '/cart/add.js',
        data: data,
        dataType: 'json',
        success: function() { 
              $.ajax({
                type: 'POST',
                url: '/cart/change.js',
                data: {
                    'id': removekey,
                    'quantity': 0,
                },
                dataType: 'json',
                success: function (cart) {
                  window.location.href = "/cart";
                },
                error: function (xhr, status, error) {
                    console.log('--error--', eval("(" + xhr.responseText + ")"))
                }
            });
          $('.mini-cart').attr('aria-expanded', 'true');
        }
      });
  }
  $( document ).ready(function() {
  $(document).on('click','.custom-submit-button',function(){   
      addItemToCart($(this).attr("data-id"), $(this).attr("data-quantity"), $(this).attr("data-key"));
  });
});



$( document ).ready(function() {
  $(document).on('click','#close_up_product',function(){   
      $('.ctm-upshell-box').hide();
  });
});


// $( document ).ready(function() {
//   $(document).on('click','.ctm-mobi-mega-3 button.menu-arrow',function(e){ 
//     e.preventDefault();
//  // $('.ctm-mobi-mega-3 button.menu-arrow').toggleClass('dropdown-rotate');
//  $('.ctm-mobi-mega-3 ul.sub-menu').toggleClass('show');
//   });
// });                              
